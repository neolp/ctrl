import React from 'react'
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button'
import Slider from '@mui/material/Slider';
import MuiInput from '@mui/material/Input';
import Switch from '@mui/material/Switch';

const Input = styled(MuiInput)`
  width: 42px;
`;

const Widget = ({ itmp, uri }) => {
  const [Zvalue, setZValue] = React.useState(30);
  const [Rvalue, setRValue] = React.useState(30);
  const [Xvalue, setXValue] = React.useState(30);
  const [Zposition, setZPosition] = React.useState(0);
  const [Rposition, setRPosition] = React.useState(0);
  const [Xposition, setXPosition] = React.useState(0);
  const [opened, setOpened] = React.useState(false);
  
  const handleOpenClose = (event) => {
    itmp.call('Xopened', event.target.checked)
    //setChecked(event.target.checked);
  };
  React.useEffect(() => {
    const onOpened = (name, v) => { setOpened(v) }
    itmp.subscribe('Xopened', onOpened)
    return () => {
      // Очистить подписку
      itmp.unsubscribe('Xopened', onOpened);
    };
  }, [itmp]);

  const handleZSliderChange = (event, newValue) => {
    setZValue(newValue);
    itmp.call(uri, newValue)
  };
  const handleRSliderChange = (event, newValue) => {
    setRValue(newValue);
    itmp.call('Rsetvalue', newValue + 115)
  };
  const handleXSliderChange = (event, newValue) => {
    setXValue(newValue);
    itmp.call('Xsetvalue', newValue)
  };

  const handleInputChange = (event) => {
    let newValue = event.target.value === '' ? '' : Number(event.target.value)
    setZValue(newValue);
    itmp.call('setvalue', newValue)
  };

  React.useEffect(() => {
    const onZpos = (name, pos) => { setZPosition(pos) }
    const onRpos = (name, pos) => { setRPosition(pos-115) }
    const onXpos = (name, pos) => { setXPosition(pos) }
    itmp.subscribe('Zposition', onZpos)
    itmp.subscribe('Rposition', onRpos)
    itmp.subscribe('Xposition', onXpos)
    return () => {
      // Очистить подписку
      itmp.unsubscribe('Zposition', onZpos);
      itmp.unsubscribe('Rposition', onRpos);
      itmp.unsubscribe('Xposition', onXpos);
    };
  },[itmp]);

  return (
    <>
      <Slider
        value={typeof Zvalue === 'number' ? Zvalue : 0}
        onChange={handleZSliderChange}
        defaultValue={30}
        // getAriaValueText={valuetext}
        valueLabelDisplay="auto"
        step={10}
        marks
        min={0}
        max={1000}
        // aria-labelledby="input-slider"
      />
      <Input
        value={Zvalue}
        size="small"
        onChange={handleInputChange}
        // onBlur={handleBlur}
        inputProps={{
          step: 10,
          min: 0,
          max: 1000,
          type: 'number',
          // 'aria-labelledby': 'input-slider',
        }}
      />
    <Button
      onClick={async () => {
        let r = await itmp.call('home',[])
        alert('clicked' + r);
      }}
    >
      HOME
      </Button>
      pos {Zposition} {Rposition}  {Xposition}
      <Slider
        value={typeof Rvalue === 'number' ? Rvalue : 0}
        onChange={handleRSliderChange}
        defaultValue={30}
        // getAriaValueText={valuetext}
        valueLabelDisplay="auto"
        step={10}
        marks
        min={-180}
        max={180}
      // aria-labelledby="input-slider"
      />
      <Button
        onClick={async () => {
          itmp.call('Rhome', [])
        }}
      >
        R HOME
      </Button>
      <Slider
        value={typeof Xvalue === 'number' ? Xvalue : 0}
        onChange={handleXSliderChange}
        defaultValue={0}
        // getAriaValueText={valuetext}
        valueLabelDisplay="auto"
        step={5}
        marks
        min={0}
        max={140}
      // aria-labelledby="input-slider"
      />
      <Switch
        checked={!!opened}
        onChange={handleOpenClose}
        // inputProps={{ 'aria-label': 'controlled' }}
      />
      <Button
        onClick={async () => {
          itmp.call('Xhome', [])
        }}
      >
        X HOME
      </Button>
  </>)
}
export default Widget