import React from 'react';
import { useEffect, useRef, useState } from "react";
import { Canvas } from "@react-three/fiber";
import { useLoader } from "@react-three/fiber";
import { Environment, OrbitControls, TransformControls } from "@react-three/drei";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { Suspense } from "react";

const Model = () => {
  const gltf = useLoader(GLTFLoader, "./pencil.gltf");
  // const gltf = useLoader(GLTFLoader, "./capacitor.gltf");
  //const gltf = useLoader(GLTFLoader, "./Poimandres.gltf");
  return (
    <>
      <primitive object={gltf.scene} scale={100.4} position={[0, 0, 0]}  />
    </>
  );
}

const Vis3 = () => {
  return (
    <div style={{height:'400px'}}>
      <Canvas>
        <Suspense fallback={null}>
          <Model />
        <OrbitControls />
        <TransformControls />
          <Environment preset="sunset" background />
        </Suspense>
      </Canvas>
    </div>
  );
}

export default Vis3