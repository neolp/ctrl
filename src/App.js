import logo from './logo.svg';
import './App.css';
import Vis from './components/sceneview'
import Vis3 from './components/scene'
import Widget from './components/widget'



function App({ itmp }) {
  
  return (
    <div className="App">
      <header className="App-header">
      <Widget itmp={itmp} uri='setvalue'></Widget>
        <Vis3></Vis3>
        <Vis></Vis>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
