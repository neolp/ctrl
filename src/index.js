import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import Itmp from 'itmpws'

console.log('itmp')
const itmp = new Itmp({uri: "ws://" + window.location.hostname + ":3006/ws/",token:'supersecret'})
itmp.connect()
itmp.call('tset','args2').then((res)=>{
  console.log('call res',res)
}).catch((err)=>{
  console.error('call err',err)
})
itmp.subscribe('cnt',(name,value)=>{
  console.log(name,'=',value)
})


ReactDOM.render(
  <React.StrictMode>
    <App itmp={itmp}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
