const path = require('path');

const { app, BrowserWindow } = require('electron');
const isDev = require('electron-is-dev');
const express = require('express')
const expressapp = express()
/* const expressWs = */ require('express-ws')(expressapp, undefined, { wsOptions: { test: '' } })
expressapp.use('/', express.static('./public'))

const hub = require('./itmphub');
//const { copyFile } = require('fs');
const RMDS = require('./devs/rmds');

let port = 3006
var server = expressapp.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`srv listening on address 2'${server.address().address}' and port ${server.address().port}`)
  
})
hub.start(expressapp,server)

//const dev = hub.getConnector('itmp.serial:///dev/ttyS0?baudRate=115200~10')
//const dev = hub.getConnector('itmp.serial://COM7?baudRate=115200~10')
//const Z = new RMDS(hub, { addr: 'itmp.serial://COM7?baudRate=115200~10', prefix: 'Z', mmstep: 0.000625 }) // линейный привод, координаты в мм
//const R = new RMDS(hub, { addr: 'itmp.serial://COM7?baudRate=115200~11', prefix: 'R', mmstep: 0.009549297, zeroshift: 0 }) // поворотный привод, координаты в градусах
//const X = new RMDS(hub, { addr: 'itmp.serial://COM7?baudRate=115200~13', prefix: 'X', mmstep: 0.017629471, zeroshift: 0 }) // поворотный привод выноса, координаты в градусах поворота
const Z = new RMDS(hub, { addr: 'fake', prefix: 'Z', mmstep: 0.000625 }) // линейный привод, координаты в мм
const R = new RMDS(hub, { addr: 'fake', prefix: 'R', mmstep: 0.009549297, zeroshift: 0 }) // поворотный привод, координаты в градусах
const X = new RMDS(hub, { addr: 'fake', prefix: 'X', mmstep: 0.017629471, zeroshift: 0 }) // поворотный привод выноса, координаты в градусах поворота


Z.start();
R.start();
X.start();


let cnt=1;
hub.setValue('cnt', cnt)
setInterval(() => {
  hub.setValue('cnt',cnt++)
}, 20000);

hub.oncall('tset',(arg)=>{
  console.log('!tset',arg)
  return arg+'!'
})

hub.oncall('setvalue', async (arg) => {
  Z.goto(arg)
})

hub.oncall('Rsetvalue', async (arg) => {
  R.goto(arg)
})

hub.oncall('Xsetvalue', async (arg) => {
  X.goto(arg)
})


hub.oncall('home', async (arg) => {
  Z.home(16*500)
})

hub.oncall('Rhome', async (arg) => {
  R.home()
})

hub.oncall('Xhome', async (arg) => {
  X.home()
})

hub.oncall('Xopened', async (arg) => {
  console.log('opened', arg)
  X.openclose(arg)
  hub.setValue('Xopened',arg)
})

function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // and load the index.html of the app.
  // win.loadFile("index.html");
  win.loadURL(
    isDev
      ? 'http://localhost:3000'
//      : 'http://localhost:3006'
      : `file://${path.join(__dirname, '../build/index.html')}`
  );
  // Open the DevTools.
  if (isDev) {
    win.webContents.openDevTools({ mode: 'detach' });
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});